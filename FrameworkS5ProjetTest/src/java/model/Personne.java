/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.HashMap;
s
/**
 *
 * @author dina
 */
public class Personne {
    int id;
    String nom;
    String prenom;

    public Personne(int id, String nom, String prenom) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPèrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    @UrlAnnotation.url(name="getListe")
        public ModelView liste() throws Exception{
        HashMap<String,Object> valeur = new HashMap();
        Personne ff = new Personne(1,"Mix","Rakoto");
        ArrayList<Personne> objet = new ArrayList<Personne>();
        valeur.put("tab",objet);
        ModelView vm = new ModelView(valeur,"liste");
        return vm;
    }

    
}
